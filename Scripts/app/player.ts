/// <reference path="../../typings/three/three.d.ts"/>


import Renderer = THREE.Renderer;
export interface IPlayerGeometry {
    fill:THREE.SphereGeometry;
    border:THREE.CircleGeometry;
}

export interface IPlayerMaterial {
    fill:THREE.Material;
    border:THREE.Material;
}

export interface IPlayerServerObject {
    socketId:string;
    position:{x:number,y:number,z:number};
    color:{ fill:number, border:number };
    size:{x:number,y:number,z:number};
}

export interface IPlayerObject {
    fill:THREE.Mesh;
    border?:THREE.Mesh;
}

export interface IPlayerKeyCodes {
    a:boolean;
    d:boolean;
    w:boolean;
    s:boolean;
    shift:boolean;
}

enum KeyboardButton {
    TAB = 9,
    ENTER = 13,
    SHIFT = 16,
    ESCAPE = 27,
    SPACE = 32,
    LEFT = 37,
    UP = 38,
    RIGHT = 39,
    DOWN = 40,
    W = 87,
    S = 83,
    A = 65,
    D = 68
}

export default class Player {
    private geometry:IPlayerGeometry;
    private material: IPlayerMaterial;
    private camDirectionVector: THREE.Vector3;
    private currentCamPosition:any;

    public camera: THREE.PerspectiveCamera;
    public camControls: any; //(THREE as any).PointerLockControls

    public label: any;
    public name: string;
    public object: IPlayerObject;
    public renderer: Renderer;
    public socket:any;
    public socketId: string;

    private keyCodes:IPlayerKeyCodes;
    private acceleration:number;
    constructor(camera: THREE.PerspectiveCamera,
                geometry:IPlayerGeometry,
                material:IPlayerMaterial,
                renderer:Renderer,
                socket:any,
                name:string){
        this.camera = camera;
        this.camControls = new (THREE as any).PointerLockControls(this.camera);
        this.camDirectionVector = new THREE.Vector3(0,0-1);
        this.camControls.enabled = true;

        this.geometry = geometry;
        this.material = material;
        this.object = this.assignObject(this.geometry, this.material);
        this.object.fill.scale.x = 10;
        this.object.fill.scale.y = 10;
        this.object.fill.scale.z = 10;

        this.name = name
        this.showName();

        this.renderer = renderer;
        this.socket = socket;
        this.socketId = socket.id;

        this.acceleration = 1;
        this.keyCodes = {
            a:false,
            d:false,
            w:false,
            s:false,
            shift:false
        }
        this.action();
    }
    private action ():void{
        this.assignWindowResizeHandler();
        this.assignKeysHandlers();
        this.watchForSocket();
    }
    private watchForSocket ():void {
        this.socket.on('player::dispose', function (data:any) {
            console.log(data);
        });
    }

    private assignKeysHandlers():void{
        document.addEventListener('keydown', this.onKeyDown.bind(this));
        document.addEventListener('keyup', this.onKeyUp.bind(this));
        document.addEventListener('mousemove', this.onMouseMove.bind(this));
    }

    private onKeyDown(event:any):void{
        switch (event.keyCode){
            case KeyboardButton.A:
                this.keyCodes.a = true;
                break;
            case KeyboardButton.D:
                this.keyCodes.d = true;
                break;
            case KeyboardButton.W:
                this.keyCodes.w = true;
                break;
            case KeyboardButton.S:
                this.keyCodes.s = true;
                break;
            case KeyboardButton.SHIFT:
                this.keyCodes.shift = true;
                break;
        }
    }

    private onKeyUp(event:any):void{
        switch (event.keyCode){
            case KeyboardButton.A:
                this.keyCodes.a = false;
                break;
            case KeyboardButton.D:
                this.keyCodes.d = false;
                break;
            case KeyboardButton.W:
                this.keyCodes.w = false;
                break;
            case KeyboardButton.S:
                this.keyCodes.s = false;
                break;
            case KeyboardButton.SHIFT:
                this.keyCodes.shift = false;
                break;
        }
    }

    private onMouseMove(event:any):void {
        this.camControls.getDirection(this.camDirectionVector);
        this.setBorderObjectRotation();
    }

    private assignWindowResizeHandler ():void {
        window.onresize = (event) => {
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix();
            this.renderer.setSize(window.innerWidth, window.innerHeight);
        };
    }

    private setBorderObjectRotation():void {       
        var vectorLeft = new THREE.Vector3(-1*this.camDirectionVector.z,0,this.camDirectionVector.x);
        var crossVector = new THREE.Vector3();        
        crossVector.crossVectors(vectorLeft, this.camDirectionVector);
        var lookAtPoint = new THREE.Vector3(
            this.object.border.position.x + crossVector.x,
            this.object.border.position.y + crossVector.y,
            this.object.border.position.z + crossVector.z);
        this.object.border.lookAt(lookAtPoint);
    }
    
    private setPlayerObjectPosition (x:number, y:number, z:number):void {
        this.object.fill.position.x = x;
        this.object.fill.position.y = y;
        this.object.fill.position.z = z;
        this.object.border.position.x = x;
        this.object.border.position.y = y;
        this.object.border.position.z = z;
        this.label.position.x = x;
        this.label.position.y = y + this.object.fill.scale.y * 1.1;
        this.label.position.z = z;
        this.setBorderObjectRotation();
        this.socket.emit('player::update', this.prepareServerObject());
    }

    public assignObject (geometry, material):IPlayerObject {
        // Remove center vertex
        //geometry.border.vertices.shift();

        return {
            fill:new THREE.Mesh(geometry.fill, material.fill),
            border:new THREE.Mesh(geometry.border, material.border)
        }
    }

    public onAdd():void {
        this.currentCamPosition = this.camControls.getObject();
        this.setPlayerObjectPosition(this.currentCamPosition.position.x, this.currentCamPosition.position.y, this.currentCamPosition.position.z);
    }

    public showName() {
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext("2d");
        ctx.font="70px Georgiasa";
        ctx.fillStyle = "white";
        ctx.fillText(this.name, 10, 90);

        let texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;
        this.label = new THREE.Mesh(new THREE.PlaneGeometry(10, 2, 10), new THREE.MeshBasicMaterial({map:texture}))
    }

    public prepareServerObject(): IPlayerServerObject{
        let serverObject = {
            socketId: this.socket.id,
            position:{ x:this.object.fill.position.x, y:this.object.fill.position.y, z:this.object.fill.position.z },
            color:{ fill:this.object.fill.material['color'], border:this.object.border.material['color'] },
            size: {x:this.object.fill.scale.x, y:this.object.fill.scale.y, z:this.object.fill.scale.y},
            name: this.name
        };
        return serverObject;
    }

    public onRender ():void {
        this.camControls.getDirection(this.camDirectionVector);
        this.currentCamPosition = this.camControls.getObject();
        this.acceleration = 1;

        if(this.keyCodes.shift)
            this.acceleration = 3;

        if(this.keyCodes.a){
            this.currentCamPosition.position.x += this.camDirectionVector.z* this.acceleration;
            this.currentCamPosition.position.z -= this.camDirectionVector.x* this.acceleration;
            this.setPlayerObjectPosition(this.currentCamPosition.position.x, this.currentCamPosition.position.y, this.currentCamPosition.position.z);
        }
        if(this.keyCodes.d){
            this.currentCamPosition.position.x -= this.camDirectionVector.z* this.acceleration;
            this.currentCamPosition.position.z += this.camDirectionVector.x* this.acceleration;
            this.setPlayerObjectPosition(this.currentCamPosition.position.x, this.currentCamPosition.position.y, this.currentCamPosition.position.z);
        }
        if(this.keyCodes.w){
            
            //this.object.fill.geometry.scale(1.1,1.1,1.1);
            this.currentCamPosition.position.x += this.camDirectionVector.x* this.acceleration;
            this.currentCamPosition.position.y += this.camDirectionVector.y* this.acceleration;
            this.currentCamPosition.position.z += this.camDirectionVector.z* this.acceleration;
            this.setPlayerObjectPosition(this.currentCamPosition.position.x, this.currentCamPosition.position.y, this.currentCamPosition.position.z);
        }
        if(this.keyCodes.s){
            this.currentCamPosition.position.x += -1 * this.camDirectionVector.x * this.acceleration;
            this.currentCamPosition.position.y += -1 * this.camDirectionVector.y* this.acceleration;
            this.currentCamPosition.position.z += -1 * this.camDirectionVector.z* this.acceleration;
            this.setPlayerObjectPosition(this.currentCamPosition.position.x, this.currentCamPosition.position.y, this.currentCamPosition.position.z);
        }
    }
}