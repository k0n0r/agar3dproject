/// <reference path="../../typings/three/three.d.ts"/>
/// <reference path="../../typings/socket.io-client/socket.io-client.d.ts"/>
import GameField from "./gameField";
import Renderer from "./renderer";
import Player, {IPlayerObject, IPlayerServerObject} from "./player";


function init(name) {
    leaderBoards()
    let socketId;
    const socket = io.connect('localhost:5000');
    socket.on('connect', function () {
        socketId = socket.id;
        console.log('connected');
    });

    let gameField: GameField = new GameField(
        new THREE.CubeGeometry(1000, 1000, 1000),
        new THREE.MeshBasicMaterial({map: THREE.ImageUtils.loadTexture('background.png'), side: THREE.DoubleSide}));
    let playersList: any = [];
    let foodsList: any = [];
    socket.on('player::create', function (serverSocketId) {
        if (serverSocketId === socketId) {

            let player = new Player(
                new THREE.PerspectiveCamera(65, window.innerWidth / window.innerHeight, 1, 2000),
                {
                    fill: new THREE.SphereGeometry(1, 200, 200),
                    border: new THREE.CircleGeometry(1, 100)
                },
                {
                    fill: new THREE.MeshBasicMaterial({
                        transparent: true,
                        opacity: 0.4,
                        color: new THREE.Color("#" + (~~(Math.random() * 0xffffff)).toString(16)).getHex(),
                        side: THREE.DoubleSide
                    }),
                    border: new THREE.MeshBasicMaterial({
                        color: new THREE.Color("#333").getHex(),
                        side: THREE.DoubleSide,
                        wireframe: true,
                        wireframeLinewidth: 3
                    })
                },
                new Renderer() as any,
                socket,
                name
            );
            playersList.push(player);
            socket.emit('player::created', player.prepareServerObject());
        }
    });
    socket.on('player::initial-load', function (data) {
        if (data.socketId === socketId) {
            for (var j = 0; j < data.playersList.length; j++) {
                if (socketId != data.playersList[j].socketId) {
                    data.playersList[j].object = assignObject(data.playersList[j]);
                    data.playersList[j].label = assignObjectLabel(data.playersList[j]);
                }
            }
            playersList = playersList.concat(data.playersList);
            for (var i = 0; i < playersList.length; i++) {
                if (playersList[i].socket && playersList[i].socket.id === socketId) {
                    gameField.scene.add(playersList[i].camControls.getObject());
                    gameField.scene.add(playersList[i].object.fill);
                    gameField.scene.add(playersList[i].object.border);
                    gameField.scene.add(playersList[i].label);
                    playersList[i].onAdd();
                } else {
                    gameField.scene.add(playersList[i].object.fill);
                    gameField.scene.add(playersList[i].label);
                    setPlayerSize(playersList[i].object.fill.scale,  playersList[i].size);
                    setPlayerPosition(playersList[i], playersList[i].position);
                }
            }

            foodsList = data.foodsList;

            for (var i = 0; i < foodsList.length; i++) {
                {
                    var obj = new THREE.Mesh(
                        new THREE.CubeGeometry(20, 20, 20),
                        new THREE.MeshStandardMaterial({
                            color: new THREE.Color("#FF0000").getHex(),
                            side: THREE.DoubleSide
                        }));

                    obj.position.x = foodsList[i].x;
                    obj.position.y = foodsList[i].y;
                    obj.position.z = foodsList[i].z;
                    foodsList[i].obj = obj;

                    gameField.scene.add(obj);
                }
            }
        }
    });

    socket.on('foods::add', function (newfood) {
        var obj = new THREE.Mesh(
            new THREE.CubeGeometry(20, 20, 20),
            new THREE.MeshStandardMaterial({
                color: new THREE.Color("#FF0000").getHex(),
                side: THREE.DoubleSide
            }));

        obj.position.x = newfood.x;
        obj.position.y = newfood.y;
        obj.position.z = newfood.z;
        newfood.obj = obj;
        foodsList.push(newfood);
        gameField.scene.add(obj);
        console.log("add");
    });

    socket.on('foods::remove', function ({id}) {
        for (var i = 0; i < foodsList.length; i++) {
            if (foodsList[i].id === id) {
                gameField.scene.remove(foodsList[i].obj);
                foodsList.splice(i, 1);
            }
        }
    });

    socket.on('player::deleted', function (playerSocketId) {
        for (var i = 0; i < playersList.length; i++) {
            if (playersList[i].socketId === playerSocketId) {
                gameField.scene.remove(playersList[i].object.fill);
                gameField.scene.remove(playersList[i].label)
                playersList.splice(i, 1);
            }
        }
    })
    socket.on('player::updated', function (playerData) {
        let playerExists;

        for (var i = 0; i < playersList.length; i++) {
            if (playerData.socketId === playersList[i].socketId) {
                playersList[i].size = playerData.size;
                setPlayerPosition(playersList[i], playerData.position);
                setPlayerSize(playersList[i].object.fill.scale, playerData.size);
                playersList[i].object.fill.scale.x = playerData.size.x;
                playersList[i].object.fill.scale.y = playerData.size.y;
                playersList[i].object.fill.scale.z = playerData.size.z;
                playersList[i].label.position.x = playerData.position.x;
                playersList[i].label.position.y = playerData.position.y + playerData.size.y * 1.1;
                playersList[i].label.position.z = playerData.position.z;
                playerExists = true
            }
            if(i !== 0) {
                playersList[i].label.lookAt(playersList[0].object.border.position)
                    playersList[i].label.scale.x = playersList[i].object.fill.scale.x / 10
                    playersList[i].label.scale.y = playersList[i].object.fill.scale.y / 10
                    playersList[i].label.scale.z = playersList[i].object.fill.scale.z / 10
            }
        }
        if (!playerExists) {
            playerData.object = assignObject(playerData);
            playerData.label = assignObjectLabel(playerData);
            gameField.scene.add(playerData.object.fill);
            gameField.scene.add(playerData.label);
            if (playerData.position)
                setPlayerPosition(playerData, playerData.position);
            if (playerData.size)
                setPlayerSize(playerData.object.fill.scale, playerData.size);
            playerData.object.fill.scale.x = playerData.size.x;
            playerData.object.fill.scale.y = playerData.size.y;
            playerData.object.fill.scale.z = playerData.size.z;
            playersList.push(playerData);
        }
    });

    setInterval(() => {
        let arr = []
        for(let i = 0; i < playersList.length; i++) {
            let player = {
                size: playersList[i].size.x,
                name: playersList[i].name
            }
            arr.push(player)
        }
        let sortedArr = arr.sort((a, b) => {
            return b.size - a.size
        });
        let board = document.getElementById('leaderBoards')
        board.innerHTML = ""
        sortedArr.forEach((player) => {
            let li = document.createElement('li')
            li.innerText = player.name + " : " + player.size
            board.appendChild(li)
        })

    }, 500)



    var render = function () {
        requestAnimationFrame(render);
        gameField.onRender();
        for (var i = 0; i < playersList.length; i++) {
            if (playersList[i].socket && playersList[i].socket.id === socketId) {
                playersList[i].onRender();
                playersList[i].renderer.renderer.render(gameField.scene, playersList[i].camera);
            }

        }
    };
    render();
}
menu()

function menu() {    // XDDDDDDDDDDDDDDDDDD
    let input = document.createElement('input')
    let button = document.createElement('button')

    input.style.position = "absolute"
    input.style.top = "10%"
    input.style.left = "45.5%"
    input.setAttribute('placeholder', 'Nick')
    button.innerText = "Start Game"
    button.style.position = "absolute"
    button.style.top = "20%"
    button.style.left = "40%"
    button.style.height = "200px"
    button.style.width = "400px"
    button.onclick = () => {
        init(input.value)
        document.body.removeChild(input)
        document.body.removeChild(button)
    }
    document.body.appendChild(input)
    document.body.appendChild(button)
}                   // XDDDDDDDDDDDDDDDDDD




function leaderBoards() {
    let div = document.createElement('div')
    let ol = document.createElement('ol')
    ol.id = "leaderBoards"
    ol.style.color = "yellow"
    div.style.position = "absolute"
    div.style.right = "0"
    div.style.top = "0"
    div.style.backgroundColor = "black"
    div.style.opacity = "0.5"
    div.style.zIndex = "2000"
    div.style.height = "300px"
    div.style.width = "150px"

    div.appendChild(ol)
    document.body.appendChild(div)
}



function assignObject(playerData: IPlayerServerObject): IPlayerObject {
    return {
        fill: new THREE.Mesh(
            new THREE.SphereGeometry(1, 200, 200),
            new THREE.MeshStandardMaterial({
                color: new THREE.Color(playerData.color.fill).getHex(),
                side: THREE.DoubleSide
            }))
    }
}

function assignObjectLabel(playerData) {
        let label
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext("2d");

        ctx.font="60px Georgia";
        ctx.fillStyle = "red";
        ctx.fillText(playerData.name, 0, 90);

        let texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;
        label = new THREE.Mesh(new THREE.PlaneGeometry(10, 2, 10), new THREE.MeshBasicMaterial({map:texture}))

        return label
}

function setPlayerPosition(playerData, newPlayerPosition): void {
    playerData.object.fill.position.x = newPlayerPosition.x;
    playerData.object.fill.position.y = newPlayerPosition.y;
    playerData.object.fill.position.z = newPlayerPosition.z;
    playerData.label.position.x = newPlayerPosition.x;
    playerData.label.position.y = newPlayerPosition.y + playerData.size.y * 1.1;
    playerData.label.position.z = newPlayerPosition.z;

}
function setPlayerSize(playerSize, newPlayerSize): void {
    playerSize.x = newPlayerSize.x;
    playerSize.y = newPlayerSize.y;
    playerSize.z = newPlayerSize.z;
}
