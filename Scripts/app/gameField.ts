export default class GameField {
    private geometry: THREE.Geometry;
    private material: THREE.MeshBasicMaterial;

    public scene: THREE.Scene;
    public backgroundObject:THREE.Mesh;

    constructor(geometry:THREE.Geometry, 
                material:THREE.MeshBasicMaterial){
        this.geometry = geometry;
        this.material = material;

        this.scene = new THREE.Scene();
        this.backgroundObject = new THREE.Mesh(this.geometry, this.material);
        var dirLight = new THREE.DirectionalLight( 0xFFFFFF, 0.8);
        var ambientLight = new THREE.AmbientLight(0xFFFFFF, 0.4);
        this.scene.add( dirLight );
        this.scene.add( ambientLight );

        this.action();
        
    }
    private action ():void{
        this.scene.add(this.backgroundObject);
    }

    public onRender ():void{
        //this.material.uniforms.amount.value += 0.05;
    }
}