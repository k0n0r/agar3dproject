export default class Renderer {
    public renderer: THREE.WebGLRenderer;
    constructor(){
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.shadowMap.enabled = true;
        this.action();
    }

    private action ():void{
        this.setSize();
        this.append();
        this.assignClickHandler();
    }

    private append ():void {
        document.body.appendChild(this.renderer.domElement);
    }

    private assignClickHandler ():void {
        this.renderer.domElement.onclick = () => this.renderer.domElement.requestPointerLock();
    }

    public setSize ():void {
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }
}
