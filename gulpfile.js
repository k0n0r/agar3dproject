var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync');
var stylus = require('gulp-stylus');
var webpack = require("webpack");
var webpackDevServer = require("webpack-dev-server");

gulp.task('scripts', function () {
	gulp.src('./Scripts/**/*.js')
		.pipe(gulp.dest('dist/js'));

    var compiler = webpack(require("./webpack.config"));
    new webpackDevServer(compiler, {
        hot: false,
        historyApiFallback: false,
        stats: {
            assets: true,
            colors: true,
            version: true,
            hash: true,
            timings: true,
            chunks: true,
            chunkModules: false
        }
    }).listen(8008, function (err) {
        if (err) {
            console.log(err);
        }
        console.log('Listening at localhost:' + 8008);
    });
});

gulp.task('assets', function () {
	gulp.src('./Assets/**/*.json')
		.pipe(gulp.dest('dist'));

	gulp.src('./Assets/**/*.jpeg')
		.pipe(gulp.dest('dist'));

    gulp.src('./Assets/**/*.jpg')
        .pipe(gulp.dest('dist'));

    gulp.src('./Assets/**/*.bmp')
        .pipe(gulp.dest('dist'));

    gulp.src('./Assets/**/*.png')
        .pipe(gulp.dest('dist'));
});

gulp.task('stylus', function () {
  return gulp.src('./Assets/css/main.styl')
    .pipe(stylus())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('browser-sync', ['nodemon'], function() {

	browserSync.init(null, {
		proxy: "http://localhost:5000",
        files: ["dist/**/*.*"],
        browser: "chrome",
        port: 7000,
	});
});

gulp.task('nodemon', function (cb) {
	
	var started = false;
	
	return nodemon({
		script: 'server.js'
	}).on('start', function () {
		// to avoid nodemon being started multiple times
		// thanks @matthisk
		if (!started) {
			cb();
			started = true; 
		} 
	});
});

gulp.task('build', ['scripts', 'stylus'], function () {});

gulp.task('default', ['build', 'watch'], function () {});

gulp.task('watch', ['browser-sync', 'assets'], function() {
    gulp.watch('./Assets/**/*.styl', ['stylus']);
    gulp.watch("*.html").on('change', browserSync.reload);
});