'use strict';

// simple express server
var express = require('express');
var app = express();
var server = app.listen(5000);

var ioFile = require('./socket.js');
ioFile(server);

app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Credentials', true);
        //res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        next();
    });

app.use(express.static('dist'));
app.get('/', function(req, res) {
    res.sendfile('./index.html');
});
