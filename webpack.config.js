module.exports = {
    entry: './Scripts/app/app.ts',
    output: {
        publicPath: __dirname + '/dist/js',
        filename: 'app.js',
        path: __dirname + '/dist/js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader'
            }
        ]
    }
};