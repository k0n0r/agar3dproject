var playersList = [];
var foodsList = [];

module.exports = function (app) {
    var io = require('socket.io')(app, {origins: '*:*'});
    setInterval(serverLoop, 200);
    
    io.on('connection', function (socket) {

        socket.emit('player::create', socket.id);
        socket.on('player::created', function (playerData){
            socket.emit('player::initial-load', { socketId:socket.id, playersList:playersList, foodsList: foodsList });
            playersList.push(playerData);
        })
        socket.on('player::update', function (playerData){
            updatePlayerBySocketId(playerData);
            io.emit('player::updated', playerData);
        })
        socket.on('disconnect', function() {
            io.emit('player::deleted', socket.id);
            deletePlayerBySocketId(socket.id);
        });
    });

    function updatePlayerBySocketId (player) {
        let playerExists;
        for(var i = 0 ; i < playersList.length;i++){
            if(playersList[i].socketId === player.socketId){
              if(playersList[i].size.x > player.size.x) {
                player.size = playersList[i].size
                playersList[i] = player;
              } else {
                playersList[i] = player;
              }
                playerExists = true;
                break;
            }
        }
        if(!playerExists){
            playersList.push(player)
        }
    }

    function deletePlayerBySocketId (socketId) {
        for(var i = 0 ; i < playersList.length;i++){
            if(playersList[i].socketId === socketId){
                playersList.splice(i,1);
                break;
            }
        }
    }

    function getDistance(obj1, obj2) {
        return Math.sqrt(Math.pow(obj1.x - obj2.x, 2) + Math.pow(obj1.y - obj2.y, 2) + Math.pow(obj1.z - obj2.z, 2));
    }


    function serverLoop() {
        if(foodsList.length < 15) {

            var newfood = {
                id: Math.random(),
                x: Math.random() * 800 - 400,
                y: Math.random() * 800 - 400,
                z: Math.random() * 800 - 400
            };
            foodsList.push(newfood);
            io.emit('foods::add', newfood);
        }

        for(let i = 0; i< playersList.length; i++) {
          for (let j = 0; j < foodsList.length; j++) {
            if (getDistance(playersList[i].position, foodsList[j]) < 20 + playersList[i].size.x) {
              playersList[i].size.x += 10;
              playersList[i].size.y += 10;
              playersList[i].size.z += 10;
              io.emit('foods::remove', { id: foodsList[j].id });
              foodsList.splice(j, 1);
              io.emit('player::updated', playersList[i]);
            }
          }
        }


        for(let i = 0; i < playersList.length; i++) {
            for(let j = 0; j < playersList.length; j++) {
                if(i !== j && getDistance(playersList[i].position, playersList[j].position) < 20 + playersList[i].size.x) {
                    if(playersList[i].size.x > playersList[j].size.x) {
                      playersList[i].size.x += playersList[j].size.x;
                      playersList[i].size.y += playersList[j].size.y;
                      playersList[i].size.z += playersList[j].size.z;
                      playersList[j].position.x = Math.random() * 800 - 400
                      playersList[j].position.y = Math.random() * 800 - 400
                      playersList[j].position.z = Math.random() * 800 - 400
                      playersList[j].size.x = 10
                      playersList[j].size.y = 10
                      playersList[j].size.z = 10
                      io.emit('player::updated', playersList[j])
                      io.emit('player::updated', playersList[i])
                    } else if(playersList[i].size.x < playersList[j].size.x) {
                      playersList[j].size.x += playersList[i].size.x;
                      playersList[j].size.y += playersList[i].size.y;
                      playersList[j].size.z += playersList[i].size.z;
                      playersList[i].position.x = Math.random() * 800 - 400
                      playersList[i].position.y = Math.random() * 800 - 400
                      playersList[i].position.z = Math.random() * 800 - 400
                      playersList[i].size.x = 10
                      playersList[i].size.y = 10
                      playersList[i].size.z = 10
                      io.emit('player::updated', playersList[i])
                      io.emit('player::updated', playersList[j])
                    }
                }
            }
        }


    }
    
}

